package com.bubnii.model;

public abstract class Plane {

    private String name;
    private int aircrew;
    private int maxRangeFlight;
    private int maxLoadCapacity;
    private int fuelStocks;

    public Plane(String name, int aircrew, int maxRangeFlight,
                 int maxLoadCapacity, int fuelStocks) {
        this.name = name;
        this.aircrew = aircrew;
        this.maxRangeFlight = maxRangeFlight;
        this.maxLoadCapacity = maxLoadCapacity;
        this.fuelStocks = fuelStocks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAircrew() {
        return aircrew;
    }

    public void setAircrew(int aircrew) {
        this.aircrew = aircrew;
    }

    public int getMaxRangeFlight() {
        return maxRangeFlight;
    }

    public void setMaxRangeFlight(int maxRangeFlight) {
        this.maxRangeFlight = maxRangeFlight;
    }

    public int getMaxLoadCapacity() {
        return maxLoadCapacity;
    }

    public void setMaxLoadCapacity(int maxLoadCapacity) {
        this.maxLoadCapacity = maxLoadCapacity;
    }

    public int getFuelStocks() {
        return fuelStocks;
    }

    public void setFuelStocks(int fuelStocks) {
        this.fuelStocks = fuelStocks;
    }

    public abstract int passengerCapacity();

    public abstract String fly();

    @Override
    public String toString() {
        return "Name plane: " + name
                + "\nNumber of aircrew: " + aircrew
                + "\nMaximum flight range: " + maxRangeFlight
                + "\nLoad capacity: " + maxLoadCapacity
                + "\nFuel stocks: " + fuelStocks;
    }
}
