package com.bubnii.model;

public class PassengerPlane extends Plane {
    private int capacitySeat;

    public PassengerPlane(String name, int aircrew, int maxRangeFlight,
                          int maxLoadCapacity, int fuelStocks, int capacitySeat) {
        super(name, aircrew, maxRangeFlight, maxLoadCapacity, fuelStocks);
        this.capacitySeat = capacitySeat;
    }

    @Override
    public int passengerCapacity() {
        return capacitySeat + getAircrew();
    }

    public String fly() {
        return "Passenger plane flies";
    }

    @Override
    public String toString() {
        return super.toString()
                + "\nPassenger capacity: " + passengerCapacity() + "\n";
    }
}