package com.bubnii.model;

public class TransportPlane extends Plane {

    private int serviceStaff;

    public TransportPlane(String name, int aircrew, int maxRangeFlight,
                          int maxLoadCapacity, int fuelStocks, int serviceStaff) {
        super(name, aircrew, maxRangeFlight, maxLoadCapacity, fuelStocks);
        this.serviceStaff = serviceStaff;
    }

    public int getServiceStaff() {
        return serviceStaff;
    }

    public void setServiceStaff(int serviceStaff) {
        this.serviceStaff = serviceStaff;
    }

    @Override
    public int passengerCapacity() {
        return getAircrew() + serviceStaff;
    }

    public String fly() {
        return "Transport Plane flies";
    }

    @Override
    public String toString() {
        return super.toString()
                + "\nPassenger capacity: " + passengerCapacity() + "\n";
    }
}
