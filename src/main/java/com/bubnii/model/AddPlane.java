package com.bubnii.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AddPlane {

    private Scanner scanner = new Scanner(System.in);
    private static List<Plane> myPlanes = new ArrayList<>();

    public List<Plane> getMyPlanes() {
        return myPlanes;
    }

    public void wayFillBaseAirplanes() {
        System.out.println("Enter 1 to add their own plane: ");
        System.out.println("Enter 2 to automatically fill: ");
        String typeOfFilling = scanner.next();
        if (typeOfFilling.equals("1")) {
            addPlane();
        } else if (typeOfFilling.equals("2")) {
            addPlanes();
        } else {
            System.out.println("Incorrect number!!!");
        }
        System.out.println("\n");
    }

    private void addPlane() {
        System.out.println("enter 1 to add a passenger plane: ");
        System.out.println("enter 2 to add a transport plane: ");
        System.out.println("Enter 3 to add a military plane: ");
        int typePlane = scanner.nextInt();
        switch (typePlane) {
            case 1:
                addPassengerPlane();
                break;
            case 2:
                addTransportPlane();
                break;
            case 3:
                addWarPlane();
                break;
            default:
                System.out.println("Incorrect number!!!");
        }
    }

    private void addPassengerPlane() {
        System.out.println("Enter name plane: ");
        String namePlane = scanner.next();
        System.out.println("Enter the number of air crew: ");
        int aircrew = scanner.nextInt();
        System.out.println("Enter the maximum flight range: ");
        int maxFlightRange = scanner.nextInt();
        System.out.println("Enter the maximum load capacity: ");
        int maxLoadCapacity = scanner.nextInt();
        System.out.println("Enter the fuel stock: ");
        int fuelStock = scanner.nextInt();
        System.out.println("Enter passenger capacity: ");
        int passengerCapacity = scanner.nextInt();
        myPlanes.add(new PassengerPlane(namePlane, aircrew, maxFlightRange, maxLoadCapacity, fuelStock, passengerCapacity));
    }

    private void addTransportPlane() {
        System.out.println("Enter name plane: ");
        String namePlane = scanner.next();
        System.out.println("Enter the number of air crew: ");
        int aircrew = scanner.nextInt();
        System.out.println("Enter the maximum flight range: ");
        int maxFlightRange = scanner.nextInt();
        System.out.println("Enter the maximum load capacity: ");
        int maxLoadCapacity = scanner.nextInt();
        System.out.println("Enter the fuel stock: ");
        int fuelStock = scanner.nextInt();
        System.out.println("Enter the number of service staff: ");
        int serviceStaff = scanner.nextInt();
        myPlanes.add(new TransportPlane(namePlane, aircrew, maxFlightRange, maxLoadCapacity, fuelStock, serviceStaff));
    }

    private void addWarPlane() {
        System.out.println("Enter name plane: ");
        String namePlane = scanner.next();
        System.out.println("Enter the number of air crew: ");
        int aircrew = scanner.nextInt();
        System.out.println("Enter the maximum flight range: ");
        int maxFlightRange = scanner.nextInt();
        System.out.println("Enter the maximum load capacity: ");
        int maxLoadCapacity = scanner.nextInt();
        System.out.println("Enter the fuel stock: ");
        int fuelStock = scanner.nextInt();
        myPlanes.add(new WarPlane(namePlane, aircrew, maxFlightRange, maxLoadCapacity, fuelStock));
    }

    private void addPlanes() {
        Plane airbusA3800 = new PassengerPlane("Airbus A380-800", 2, 14060, 256000, 117340, 550);
        Plane boeing747 = new PassengerPlane("Boeing 747", 2, 17500, 210030, 147500, 405);
        Plane hughes = new TransportPlane("Hughes H-4 Hercules", 4, 15000, 210900, 130500, 20);
        Plane airbusA300 = new PassengerPlane("Airbus A300-600ST Beluga", 3, 13900, 310000, 160500, 360);
        Plane an225 = new TransportPlane("АН-225 Мрія", 6, 16000, 350000, 195000, 70);
        Plane an124 = new WarPlane("АН-124 Руслан", 8, 18200, 245000, 140500);
        Plane lockheed = new WarPlane("Lockheed C-5 Galaxy", 7, 16500, 234600, 135500);
        Plane an22 = new WarPlane("АН-22 Антей", 7, 14600, 210050, 172100);
        myPlanes.add(airbusA3800);
        myPlanes.add(boeing747);
        myPlanes.add(hughes);
        myPlanes.add(airbusA300);
        myPlanes.add(an22);
        myPlanes.add(an124);
        myPlanes.add(an225);
        myPlanes.add(lockheed);
    }
}
