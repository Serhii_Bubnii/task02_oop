package com.bubnii.model;

public class WarPlane extends Plane {

    public WarPlane(String name, int aircrew, int maxRangeFlight,
                    int maxLoadCapacity, int fuelStocks) {
        super(name, aircrew, maxRangeFlight, maxLoadCapacity, fuelStocks);
    }

    @Override
    public int passengerCapacity() {
        return getAircrew();
    }

    @Override
    public String fly() {
        return "War Plane flies";
    }

    @Override
    public String toString() {
        return super.toString()
                + "\nPassenger capacity: " + passengerCapacity() + "\n";
    }
}
