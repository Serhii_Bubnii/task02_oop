package com.bubnii.controller;

import com.bubnii.model.Plane;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AirlineController {

    private List<Plane> planes;

    public AirlineController(List<Plane> planes) {
        this.planes = planes;
    }

    public void showAirplanesInAirline() {
        for (Plane plane : planes) {
            System.out.println(plane);
        }
    }

    public void getSumOfLoadCapacity() {
        int sumLoad = 0;
        for (Plane plane : planes) {
            sumLoad += plane.getMaxLoadCapacity();
        }
        System.out.println("Sum of load capacity = " + sumLoad);
    }

    public void getSumPassengerCapacity() {
        int sumSeat = 0;
        for (Plane plane : planes) {
            sumSeat += plane.passengerCapacity();
        }
        System.out.println("Sum of passenger capacity = " + sumSeat);
    }

    public void sortMaxRangeFlight() {
        Collections.sort(planes, new Comparator<Plane>() {
            @Override
            public int compare(Plane o1, Plane o2) {
                return Integer.compare(o2.getMaxRangeFlight(), o1.getMaxRangeFlight());
            }
        });
        for (Plane plane : planes) {
            System.out.println(plane);
        }
    }

    public void getByFuelConsumption(int with, int to) {
        for (Plane plane : planes) {
            if (plane.getFuelStocks() > with && plane.getFuelStocks() < to) {
                System.out.println(plane);
            }
        }
    }
}
