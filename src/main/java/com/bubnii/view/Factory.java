package com.bubnii.view;

import com.bubnii.controller.AirlineController;
import com.bubnii.model.AddPlane;
import com.bubnii.model.Plane;

import java.util.List;
import java.util.Scanner;

public class Factory {

    private List<Plane> myPlanes = new AddPlane().getMyPlanes();
    private AirlineController airline = new AirlineController(myPlanes);
    private Scanner scanner = new Scanner(System.in);
    private AddPlane addPlane = new AddPlane();

    public void myFactory() {
        while (true) {
            System.out.println("enter 1 to add planes");
            System.out.println("Enter 2 to display all available airplanes");
            System.out.println("Enter 3 to display the amount of seats");
            System.out.println("Enter 4 to display the amount of load capacity");
            System.out.println("Enter 5 to display sort aircraft " +
                    "by the flight range");
            System.out.println("Enter 6 to display find an airplane" +
                    " that meets a given range of fuel consumption parameters");
            System.out.println("Enter 0 to exit");

            int numberFeature = scanner.nextInt();

            switch (numberFeature) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    addPlane.wayFillBaseAirplanes();
                    break;
                case 2:
                    airline.showAirplanesInAirline();
                    break;
                case 3:
                    airline.getSumPassengerCapacity();
                    break;
                case 4:
                    airline.getSumOfLoadCapacity();
                    break;
                case 5:
                    airline.sortMaxRangeFlight();
                    break;
                case 6:
                    sampleFromRange();
                    break;
                default:
                    System.out.println("Incorrect number!!!");
            }
            System.out.println("\n");
        }
    }

    private void sampleFromRange() {
        int startInterval;
        int finishRange;
        System.out.print("Enter the start of the range: ");
        startInterval = scanner.nextInt();
        System.out.println();
        System.out.print("Enter the finish of the range: ");
        finishRange = scanner.nextInt();
        airline.getByFuelConsumption(startInterval, finishRange);
    }
}
